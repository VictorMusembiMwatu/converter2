package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static java.lang.Double.parseDouble;

public class MainActivity extends AppCompatActivity {
    private EditText editText;
    private EditText editText2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         editText = (EditText) findViewById(R.id.edtText);
         editText2 = (EditText) findViewById(R.id.editText);


    }
    public void Convert(View v){
        double mm= parseDouble(editText.getText().toString());
        double inches=mm/25.4;

        editText2.setText(Double.toString(inches));
    }


        public void Exit(View v) {

            finish();
            System.exit(0);
        }
    }
